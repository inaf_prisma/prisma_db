<p align="center">
  <img src="https://i.imgur.com/IHjGJBk.png" />
</p>

# PRISMA Database
This repo contains the logic necessary to run the database (MySQL/InnoDB) node

**install**:
        $ make build

**deploy**:
        $ docker start prisma/db
        