#immagine di partenza mysql 8
FROM mysql:8

#viene strutturato il database (solo se file system vuoto)
COPY ./CSV_initial_dataset/ /CSV_initial_dataset
COPY ./dump_db/  /docker-entrypoint-initdb.d
