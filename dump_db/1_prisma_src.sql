-- Generated: 2021-01-04 16:14
-- Version: 2.0
-- Project: N3 S.r.l per Istituto Nazionale di Astrofisica - Progetto PRISMA
-- Author: Andrea Novati

SET time_zone = '+01:00';

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `inaf_prisma` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin ;

CREATE TABLE IF NOT EXISTS `inaf_prisma`.`pr_core_person` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `oid` VARCHAR(32) NULL DEFAULT NULL,
  `username` VARCHAR(100) NULL DEFAULT NULL,
  `password` VARCHAR(100) NULL DEFAULT NULL,
  `email` VARCHAR(100) NULL DEFAULT NULL,
  `title` VARCHAR(10) NULL DEFAULT NULL,
  `first_name` VARCHAR(100) NULL DEFAULT NULL,
  `middle_name` VARCHAR(100) NULL DEFAULT NULL,
  `last_name` VARCHAR(100) NULL DEFAULT NULL,
  `suffix` VARCHAR(10) NULL DEFAULT NULL,
  `company` VARCHAR(100) NULL DEFAULT NULL,
  `job_title` VARCHAR(100) NULL DEFAULT NULL,
  `web_page_address` VARCHAR(100) NULL DEFAULT NULL,
  `im_address` VARCHAR(100) NULL DEFAULT NULL,
  `phone` VARCHAR(45) NULL DEFAULT NULL,
  `address` VARCHAR(100) NULL DEFAULT NULL,
  `postcode` VARCHAR(45) NULL DEFAULT NULL,
  `number` VARCHAR(45) NULL DEFAULT NULL,
  `city` VARCHAR(45) NULL DEFAULT NULL,
  `province` VARCHAR(100) NULL DEFAULT NULL,
  `country` VARCHAR(100) NULL DEFAULT NULL,
  `timezone` VARCHAR(100) NULL DEFAULT 'Europe/London',
  `created_by` INT(11) NULL DEFAULT NULL,
  `modified_by` INT(11) NULL DEFAULT NULL,
  `assigned` INT(11) NULL DEFAULT NULL,
  `is_station_referer` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  `is_administrator` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `erased` TINYINT(1) UNSIGNED NOT NULL,
  `last_update` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

CREATE TABLE IF NOT EXISTS `inaf_prisma`.`pr_region` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `oid` VARCHAR(32) NULL DEFAULT NULL,
  `state` VARCHAR(2) NOT NULL DEFAULT 'IT',
  `code` VARCHAR(2) NOT NULL,
  `name` VARCHAR(80) NOT NULL,
  `modified_by` INT(11) UNSIGNED NOT NULL,
  `created_by` INT(11) UNSIGNED NOT NULL,
  `assigned` INT(11) UNSIGNED NOT NULL,
  `erased` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `last_update` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

CREATE TABLE IF NOT EXISTS `inaf_prisma`.`pr_station` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `oid` VARCHAR(32) NULL DEFAULT NULL,
  `region_id` INT(11) UNSIGNED NOT NULL,
  `code` VARCHAR(8) NOT NULL,
  `sequence_number` VARCHAR(3) NOT NULL COMMENT 'used to identify station under the same region.',
  `altitude` VARCHAR(100) NULL DEFAULT NULL,
  `longitude` FLOAT(12,8) NULL DEFAULT NULL,
  `latitude` FLOAT(12,8) NULL DEFAULT NULL,
  `note` TEXT NULL DEFAULT NULL,
  `nickname` VARCHAR(100) NULL DEFAULT NULL,
  `registration_date` TIMESTAMP NULL DEFAULT NULL,
  `active` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 ,
  `modified_by` INT(11) UNSIGNED NOT NULL,
  `created_by` INT(11) UNSIGNED NOT NULL,
  `assigned` INT(11) UNSIGNED NOT NULL,
  `erased` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `last_update` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_station_region1_idx` (`region_id` ASC),
  CONSTRAINT `fk_station_region1`
    FOREIGN KEY (`region_id`)
    REFERENCES `inaf_prisma`.`pr_region` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


CREATE TABLE IF NOT EXISTS `inaf_prisma`.`pr_system_configuration` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `oid` VARCHAR(32) NULL DEFAULT NULL,
  `parameter_name` TEXT NOT NULL,
  `parameter_value` TEXT NOT NULL,
  `modified_by` INT(11) UNSIGNED NOT NULL,
  `created_by` INT(11) UNSIGNED NOT NULL,
  `assigned` INT(11) UNSIGNED NOT NULL,
  `erased` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `last_update` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

CREATE TABLE IF NOT EXISTS `inaf_prisma`.`pr_node` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `oid` VARCHAR(32) NULL DEFAULT NULL,
  `station_id` INT(11) UNSIGNED NOT NULL,
  `MAC_address` VARCHAR(45) NULL DEFAULT NULL,
  `hostname` VARCHAR(100) NULL DEFAULT NULL,
  `CName` VARCHAR(100) NOT NULL COMMENT 'a station can have multiple nodes.. each with one ore multiple camera attached',
  `freeture_configuration_file` VARCHAR(100) NULL DEFAULT NULL,
  `ovpnfile` VARCHAR(100) NULL DEFAULT NULL,
  `interval_running_DEA` INT(11) NULL DEFAULT NULL,
  `relative_path` VARCHAR(100) NOT NULL,
  `modified_by` INT(11) UNSIGNED NOT NULL,
  `created_by` INT(11) UNSIGNED NOT NULL,
  `assigned` INT(11) UNSIGNED NOT NULL,
  `erased` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `last_update` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_node_station1_idx` (`station_id` ASC),
  CONSTRAINT `fk_node_station1`
    FOREIGN KEY (`station_id`)
    REFERENCES `inaf_prisma`.`pr_station` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

CREATE TABLE IF NOT EXISTS `inaf_prisma`.`pr_camera` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `oid` VARCHAR(32) NULL DEFAULT NULL,
  `node_id` INT(11) UNSIGNED NOT NULL,
  `code` VARCHAR(8) NOT NULL,
  `config_file` VARCHAR(100) NULL DEFAULT NULL,
  `mask_file` VARCHAR(100) NULL DEFAULT NULL,
  `model` VARCHAR(100) NULL DEFAULT NULL,
  `modified_by` INT(11) UNSIGNED NOT NULL,
  `created_by` INT(11) UNSIGNED NOT NULL,
  `assigned` INT(11) UNSIGNED NOT NULL,
  `erased` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `last_update` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_camera_node1_idx` (`node_id` ASC),
  UNIQUE INDEX `config_file_copy1_UNIQUE` (`code` ASC),
  CONSTRAINT `fk_camera_node1`
    FOREIGN KEY (`node_id`)
    REFERENCES `inaf_prisma`.`pr_node` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

CREATE TABLE IF NOT EXISTS `inaf_prisma`.`pr_event` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `oid` VARCHAR(32) NULL DEFAULT NULL,
  `code` VARCHAR(15) NOT NULL,
  `to_process` TINYINT(1) NOT NULL,
  `modified_by` INT(11) UNSIGNED NOT NULL,
  `created_by` INT(11) UNSIGNED NOT NULL,
  `assigned` INT(11) UNSIGNED NOT NULL,
  `erased` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `last_update` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `code_UNIQUE` (`code` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

CREATE TABLE IF NOT EXISTS `inaf_prisma`.`pr_log_program_file` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `oid` VARCHAR(32) NULL DEFAULT NULL,
  `timestamp` DATETIME NOT NULL,
  `type` VARCHAR(45) NOT NULL COMMENT 'INFO, ERROR, WARNING',
  `level` INT(11) NOT NULL,
  `text` TEXT NOT NULL,
  `modified_by` INT(11) UNSIGNED NOT NULL,
  `created_by` INT(11) UNSIGNED NOT NULL,
  `assigned` INT(11) UNSIGNED NOT NULL,
  `erased` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `last_update` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

CREATE TABLE IF NOT EXISTS `inaf_prisma`.`pr_detection` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `oid` VARCHAR(32) NULL DEFAULT NULL,
  `node_id` INT(11) UNSIGNED NOT NULL,
  `event_id` INT(11) UNSIGNED NULL DEFAULT NULL,
  `inserted_timestamp` TIMESTAMP NULL DEFAULT NULL,
  `detected_timestamp` TIMESTAMP NULL DEFAULT NULL,
  `is_fake` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `modified_by` INT(11) UNSIGNED NOT NULL,
  `created_by` INT(11) UNSIGNED NOT NULL,
  `assigned` INT(11) UNSIGNED NOT NULL,
  `erased` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `last_update` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_detection_event1_idx` (`event_id` ASC),
  INDEX `fk_detection_node_idx` (`node_id` ASC),
  CONSTRAINT `fk_detection_event1`
    FOREIGN KEY (`event_id`)
    REFERENCES `inaf_prisma`.`pr_event` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_detection_node`
    FOREIGN KEY (`node_id`)
    REFERENCES `inaf_prisma`.`pr_node` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

CREATE TABLE IF NOT EXISTS `inaf_prisma`.`pr_refer_to` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `core_person_id` INT(11) UNSIGNED NOT NULL,
  `station_id` INT(11) UNSIGNED NOT NULL,
  `erased` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `last_update` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_core_person_has_station_station1_idx` (`station_id` ASC),
  INDEX `fk_core_person_has_station_core_person1_idx` (`core_person_id` ASC),
  CONSTRAINT `fk_core_person_has_station_core_person1`
    FOREIGN KEY (`core_person_id`)
    REFERENCES `inaf_prisma`.`pr_core_person` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_core_person_has_station_station1`
    FOREIGN KEY (`station_id`)
    REFERENCES `inaf_prisma`.`pr_station` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

CREATE TABLE IF NOT EXISTS `inaf_prisma`.`pr_observed_by` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `event_id` INT(11) UNSIGNED NOT NULL,
  `detection_id` INT(11) UNSIGNED NOT NULL,
  `erased` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `last_update` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_event_has_detection_detection1_idx` (`detection_id` ASC),
  INDEX `fk_event_has_detection_event1_idx` (`event_id` ASC),
  CONSTRAINT `fk_event_has_detection_event1`
    FOREIGN KEY (`event_id`)
    REFERENCES `inaf_prisma`.`pr_event` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_event_has_detection_detection1`
    FOREIGN KEY (`detection_id`)
    REFERENCES `inaf_prisma`.`pr_detection` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

CREATE TABLE IF NOT EXISTS `inaf_prisma`.`pr_email_alert` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `event_id` INT(11) UNSIGNED NOT NULL,
  `core_person_id` INT(11) UNSIGNED NOT NULL,
  `erased` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `last_update` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_event_has_core_person_core_person1_idx` (`core_person_id` ASC),
  INDEX `fk_event_has_core_person_event1_idx` (`event_id` ASC),
  CONSTRAINT `fk_event_has_core_person_event1`
    FOREIGN KEY (`event_id`)
    REFERENCES `inaf_prisma`.`pr_event` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_event_has_core_person_core_person1`
    FOREIGN KEY (`core_person_id`)
    REFERENCES `inaf_prisma`.`pr_core_person` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

CREATE TABLE IF NOT EXISTS `inaf_prisma`.`pr_event_execution_history` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `oid` VARCHAR(32) NULL DEFAULT NULL,
  `event_id` INT(11) UNSIGNED NOT NULL,
  `execution_datetime` DATETIME NOT NULL,
  `config_parameters` TEXT NULL DEFAULT NULL,
  `stdout` TEXT NULL DEFAULT NULL,
  `stderr` TEXT NULL DEFAULT NULL,
  `modified_by` INT(11) UNSIGNED NOT NULL,
  `created_by` INT(11) UNSIGNED NOT NULL,
  `assigned` INT(11) UNSIGNED NOT NULL,
  `erased` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `last_update` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`, `event_id`, `execution_datetime`),
  INDEX `fk_eeh_event1_idx` (`event_id` ASC),
  CONSTRAINT `fk_eeh_event1`
    FOREIGN KEY (`event_id`)
    REFERENCES `inaf_prisma`.`pr_event` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

CREATE TABLE IF NOT EXISTS `inaf_prisma`.`pr_event_log` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `oid` VARCHAR(32) NULL DEFAULT NULL,
  `event_id` INT(11) UNSIGNED NOT NULL,
  `type` TEXT NULL DEFAULT NULL,
  `description` TEXT NULL DEFAULT NULL,
  `parameters` TEXT NULL DEFAULT NULL,
  `log_timestamp` DATETIME NOT NULL,
  `modified_by` INT(11) UNSIGNED NOT NULL,
  `created_by` INT(11) UNSIGNED NOT NULL,
  `assigned` INT(11) UNSIGNED NOT NULL,
  `erased` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `last_update` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`, `event_id`),
  INDEX `fk_el_event1_idx` (`event_id` ASC),
  CONSTRAINT `fk_el_event1`
    FOREIGN KEY (`event_id`)
    REFERENCES `inaf_prisma`.`pr_event` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

CREATE TABLE IF NOT EXISTS `inaf_prisma`.`pr_user_configuration` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `oid` VARCHAR(32) NULL,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `parameter_name` TEXT NOT NULL,
  `parameter_value` TEXT NOT NULL,
  `modified_by` INT(11) UNSIGNED NOT NULL,
  `created_by` INT(11) UNSIGNED NOT NULL,
  `assigned` INT(11) UNSIGNED NOT NULL,
  `erased` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `last_update` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`, `user_id`),
  CONSTRAINT `fk_cf_core_person4`
    FOREIGN KEY (`user_id`)
    REFERENCES `inaf_prisma`.`pr_core_person` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

CREATE TABLE IF NOT EXISTS `inaf_prisma`.`pr_calibration_execution_history` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `oid` VARCHAR(32) NULL DEFAULT NULL,
  `camera_id` INT(11) UNSIGNED NOT NULL,
  `date` INT(8) NOT NULL,
  `execution_datetime` DATETIME NOT NULL,
  `monthly_or_daily` TINYINT(1) NOT NULL,
  `config_parameters` TEXT NULL DEFAULT NULL,
  `stdout` TEXT NULL DEFAULT NULL,
  `stderr` TEXT NULL DEFAULT NULL,
  `modified_by` INT(11) UNSIGNED NOT NULL,
  `created_by` INT(11) UNSIGNED NOT NULL,
  `assigned` INT(11) UNSIGNED NOT NULL,
  `erased` TINYINT(1) UNSIGNED NOT NULL,
  `last_update` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`, `camera_id`, `date`, `execution_datetime`),
  INDEX `fk_ceh_camera1_idx` (`camera_id` ASC),
  CONSTRAINT `fk_ceh_camera1`
    FOREIGN KEY (`camera_id`)
    REFERENCES `inaf_prisma`.`pr_camera` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
