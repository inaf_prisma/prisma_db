
LOAD DATA INFILE '/CSV_initial_dataset/pr_region.csv' 
INTO TABLE inaf_prisma.pr_region 
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;

LOAD DATA INFILE '/CSV_initial_dataset/pr_core_person.csv' 
INTO TABLE inaf_prisma.pr_core_person 
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;

LOAD DATA INFILE '/CSV_initial_dataset/pr_station.csv' 
INTO TABLE inaf_prisma.pr_station 
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;

LOAD DATA INFILE '/CSV_initial_dataset/pr_node.csv' 
INTO TABLE inaf_prisma.pr_node 
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;

LOAD DATA INFILE '/CSV_initial_dataset/pr_camera.csv' 
INTO TABLE inaf_prisma.pr_camera 
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;

LOAD DATA INFILE '/CSV_initial_dataset/pr_user_configuration.csv' 
INTO TABLE inaf_prisma.pr_user_configuration 
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;

LOAD DATA INFILE '/CSV_initial_dataset/pr_event.csv' 
INTO TABLE inaf_prisma.pr_event 
FIELDS TERMINATED BY ',' 
OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;

LOAD DATA INFILE '/CSV_initial_dataset/pr_event_log.csv' 
INTO TABLE inaf_prisma.pr_event_log 
FIELDS TERMINATED BY ',' 
OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;

LOAD DATA INFILE '/CSV_initial_dataset/pr_system_configuration.csv' 
INTO TABLE inaf_prisma.pr_system_configuration 
FIELDS TERMINATED BY ',' 
OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;

LOAD DATA INFILE '/CSV_initial_dataset/pr_calibration_execution_history.csv' 
INTO TABLE inaf_prisma.pr_calibration_execution_history 
FIELDS TERMINATED BY ',' 
OPTIONALLY ENCLOSED BY '"' TERMINATED BY ','
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;

LOAD DATA INFILE '/CSV_initial_dataset/pr_detection.csv' 
INTO TABLE inaf_prisma.pr_detection 
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;

LOAD DATA INFILE '/CSV_initial_dataset/pr_email_alert.csv' 
INTO TABLE inaf_prisma.pr_email_alert 
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;

LOAD DATA INFILE '/CSV_initial_dataset/pr_event_execution_history.csv' 
INTO TABLE inaf_prisma.pr_event_execution_history 
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;

LOAD DATA INFILE '/CSV_initial_dataset/pr_log_program_file.csv' 
INTO TABLE inaf_prisma.pr_log_program_file 
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;

LOAD DATA INFILE '/CSV_initial_dataset/pr_observed_by.csv' 
INTO TABLE inaf_prisma.pr_observed_by 
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;

LOAD DATA INFILE '/CSV_initial_dataset/pr_refer_to.csv' 
INTO TABLE inaf_prisma.pr_refer_to 
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;